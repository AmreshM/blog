const router = require('express').Router();
const bcrypt = require('bcrypt');
const { User, validate } = require('../models/users');

router.post('/', async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    let user = await User.findOne({ email: req.body.email });
    if (user) return res.status(400).send('User Already registered.');

    const {
        username,
        firstname,
        lastname,
        email,
        password,
    } = req.body;
    user = new User({
        username,
        firstname,
        lastname,
        email,
        password,
    });
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
    const token = user.genAuthToken();
    res.header('x-auth-token', token).send(user);
});

module.exports = router;
