const express = require('express');
const Users = require('./users');
const Auth = require('./auth');
const Posts = require('./posts')

module.exports = function (app) {
    app.use(express.json());
    app.use('/api/user', Users);
    app.use('/api/auth', Auth);
    app.use('/api/posts',Posts);
};
