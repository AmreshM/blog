const router = require('express').Router();
const { User } = require('../models/users')
const { Posts, validate, validatePut, validateId } = require('../models/posts');

const auth = require('../middlewares/auth');

router.get('/', async (req, res) => {
    const posts = await Posts.find().sort({ date: -1 });
    res.send(posts);
});
router.get('/user/:id', auth, async (req, res) => {
    const { error } = validateId({ id: req.params.id });
    if (error) return res.status(400).send(error.details[0].message);
    const posts = await Posts.find({ 'author._id': req.params.id })
        .sort({ date: -1 })
    res.send(posts);
});
router.get('/:id', async (req, res) => {
    const { error } = validateId({ id: req.params.id });
    if (error) return res.status(400).send(error.details[0].message);
    const post = await Posts.findById(req.params.id)
    res.send(post);
});

router.post('/', auth, async (req, res) => {
    const { error } = validate(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    let user = await User.findById(req.user._id)
    let post = new Posts({
        title: req.body.title,
        description: req.body.description,
        author: {
            _id: user._id,
            firstname: user.firstname,
            lastname: user.lastname
        }
    })
    post = await post.save();
    res.send(post)
});

router.put('/', auth, async (req, res) => {
    const { error } = validatePut(req.body);
    if (error) return res.status(400).send(error.details[0].message);
    let postAuthor = await Posts.findById(req.body.id)
    if (JSON.stringify(req.user._id) !== JSON.stringify(postAuthor.author._id)) return res.status(403).send('Access Denied.')
    let updateData = {
        title: req.body.title, description: req.body.description
    };
    for (let data in updateData) if (!updateData[data]) delete updateData[data];
    let post = await Posts.findOneAndUpdate({ _id: req.body.id }, updateData, { new: true })
    res.send(post);

})

router.delete('/:id', auth, async (req, res) => {
    const { error } = validateId({ id: req.params.id });
    if (error) return res.status(400).send(error.details[0].message);
    let user = await User.findById(req.user._id)
    if (!user) return res.status(400).send('Bad request.');
    let post = await Posts.findById(req.params.id)    
    let isValidUser = !!req.user.isAdmin || JSON.stringify(post.author._id) === JSON.stringify(user._id)    
    if (!isValidUser) return res.status(403).send('Access Denied.')
    console.log(isValidUser)
    console.log(user)
    post.deleteOne( {"_id": req.params.id});
    res.send(post)
})




module.exports = router;
