const mongoose = require('mongoose');
const Joi = require('joi');
const Posts = mongoose.model(
    'posts',
    new mongoose.Schema({
        title: {
            type: String,
            minlength: 20,
            required: true,
        },
        description: {
            type: String,
            minlength: 100,
            required: true,
        },
        author: {
            _id: mongoose.Schema.Types.ObjectId,
            firstname: String,
            lastname: String
        },
        date: {
            type: Date,
            default: Date.now,
        },
    }),
);

const validatePosts = (posts) => {
    const schema = {
        title: Joi.string().min(20).required(),
        description: Joi.string().min(100).required(),
    };

    return Joi.validate(posts, schema);
};
const validatePut = (posts) => {
    const schema = {
        title: Joi.string().min(20),
        description: Joi.string().min(100),
        id: Joi.objectId().required()
    };

    return Joi.validate(posts, schema);
};

const validateId = (id) => {
    const schema = {
        id: Joi.objectId().required(),

    };

    return Joi.validate(id, schema);
};


exports.Posts = Posts;
exports.validate = validatePosts;
exports.validatePut = validatePut;
exports.validateId = validateId
