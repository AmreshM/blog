import React from 'react';

import ViewPost from '../../routes/viewpost';

function Post(props) {
  let userData = JSON.parse(localStorage.getItem('userdata'));
  return (
    <ViewPost
      id={props.match.params.id}
      history={props.history}
      userData={userData}
    />
  );
}

export default Post;
