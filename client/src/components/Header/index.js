import React from 'react';
import { Link } from 'react-router-dom';
import { PageHeader, Button, Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';

import './index.css';

import { routes } from '../../constants/route';
import {instance } from '../../api/axios'

const Extra = profile => {
  if (!profile) {
    return (
      <Button type="link">
        <Link to={routes.LOGIN}>Login</Link>
        <span>/</span>
        <Link to={routes.REGISTER}>Register</Link>
      </Button>
    );
  } else {
    return <Profile username={profile.username} />;
  }
};

const Header = props => {
  const { headerTabs, profile, tabClick } = props;
  return (
    <div className="site-page-header-ghost-wrapper">
      <PageHeader
        ghost={false}
        title={
          <Menu
            mode="horizontal"
            defaultSelectedKeys={['0']}
            selectedKeys={[String(props.i)]}
          >
            {headerTabs &&
              headerTabs.headerTabs.map((val, i) => {
                return (
                  <Menu.Item
                    key={i}
                    style={{ marginRight: 20 }}
                    onClick={() => tabClick(val, i)}
                  >
                    {val}
                  </Menu.Item>
                );
              })}
          </Menu>
        }
        extra={Extra(profile)}
      ></PageHeader>
    </div>
  );
};

const menu = (
  <Menu>
    <Menu.Item key="0">
      <Link
        to="/"
        onClick={() => {
          instance.defaults.headers['x-auth-token'] = ''
          localStorage.clear();         
        }}
      >
        LogOut
      </Link>
    </Menu.Item>
  </Menu>
);

const Profile = props => {
  return (
    <Dropdown overlay={menu} trigger={['click']}>
      <Button
        type="link"
        className="ant-dropdown-link"
        onClick={e => e.preventDefault()}
      >
        {props.username} <DownOutlined />
      </Button>
    </Dropdown>
  );
};

export default Header;
