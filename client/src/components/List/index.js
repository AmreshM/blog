import React, { useState, useEffect } from 'react';
import { List, Avatar, message, Spin } from 'antd';
import { Link, useHistory } from 'react-router-dom';

import { routes } from '../../constants/route';
import { Get } from '../../api/axios';

function Lists(props) {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(true)
  const history = useHistory();
  const key = 'update'
  useEffect(() => {
    let isSubscribed = true;
    setData([])
    switch (props.value) {
      case 'New post':
        history.push(routes.CREATEPOST);
        break;
      case 'My Post list':
        message.loading({ content: 'Loading...', key });
        Get(`/posts/user/${JSON.parse(localStorage.getItem('userdata'))._id}`)
          .then(res => {
            message.success({ content: 'Loaded!', key, duration: 1 });
            setIsLoading(false)
            if (isSubscribed) setData(res.data);
          })
          .catch(error => {
            message.error({ content: error, key, duration: 2 })
            setIsLoading(false)
            if (isSubscribed) console.log('error=>', error);
          });
        break;
      case 'Review request':

        break;
      case 'Review spams':

        break;
      case 'Users list':

        break;
      case 'All Posts':
        message.loading({ content: 'Loading...', key });
        Get('/posts')
          .then(res => {
            message.success({ content: 'Loaded!', key, duration: 1 });
            setIsLoading(false)
            if (isSubscribed) setData(res.data);
          })
          .catch(error => {
            message.error({ content: error, key, duration: 2 })
            setIsLoading(false)
            if (isSubscribed) console.log('error=>', error);
          });
        break;
      default:
        break;
    }
    return () => {
      isSubscribed = false;
    };
  }, [props.value, history]);
  return (
    isLoading ? <Spin style={{ position: 'absolute', top: "50%", left: "50%" }} size="large" /> : <List
      itemLayout="horizontal"
      dataSource={data}
      renderItem={item => (
        <List.Item>
          <List.Item.Meta
            avatar={
              <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
            }
            title={<Link to={routes.POST + item._id}>{item.title}</Link>}
            description={
              <div>
                <p>{item.description}</p>
                <span style={{ fontWeight: 'bolder' }}>
                  {item.author.firstname}
                </span>
                <span style={{ fontWeight: 'bolder', paddingLeft: 20 }}>
                  {new Date(item.date).toUTCString()}
                </span>
              </div>
            }
          />
        </List.Item>
      )}
    />
  );
}

export default Lists;
