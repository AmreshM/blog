import React from 'react';
import CustomRoute from './routes';

function App() {
  return (
    <div>
      <CustomRoute />
    </div>
  );
}

export default App;
