import {auth} from '../action/actiontypes'
export const loginReducer = (state=[],action)=>{
   
    switch (action.type) {
        case auth.AUTH_SUCCESS:        
            return action.payload;
        case auth.AUTH_ERROR:            
            return ['something error']    
        default:            
            return state
    }
}