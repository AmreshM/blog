import {auth} from './actiontypes'

export const AuthSuccessAction = (data) =>{
    return {
        type:auth.AUTH_SUCCESS,
        payload:data
    }
}

export const AuthErrorAction = (data)=>{
    return {
        type:auth.AUTH_ERROR,
        payload:data
    }
}