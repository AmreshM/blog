import React, { useState } from 'react';
import Header from '../../components/Header';
import Lists from '../../components/List';

const headerTabs = {
  headerTabs: ['All Posts', 'New post', 'My Post list'],
};

function UserP(props) {
  const [tab, setTab] = useState('All Posts');
  const [index, setIndex] = useState(0);
  const handleTabClick = (val, index) => {
    setIndex(index);
    setTab(val);
  };
  return (
    <div>
      <Header
        headerTabs={headerTabs}
        profile={props.profile}
        tabClick={handleTabClick}
        i={index}
      />
      <Lists value={tab} />
    </div>
  );
}

export default UserP;
