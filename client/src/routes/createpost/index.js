import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Card, Col, Row, Input, Button } from 'antd';

import { Post, Put } from '../../api/axios';
import { routes } from '../../constants/route';

function CreatePost() {
  const history = useHistory();
  const [data, setData] = useState({ title: '', description: '' });
  const [isEdit, setIsEdit] = useState(false);
  const handleSubmit = () => {
    if (isEdit) {
      Put('/posts', { ...data, id: history.location.state._id })
        .then(res => {
          console.log(res);
          history.replace(routes.HOME);
        })
        .catch(error => {
          console.log('error=>', error);
        });
    } else {
      Post('/posts', data)
        .then(res => {
          console.log(res);
          history.replace(routes.HOME);
        })
        .catch(error => {
          console.log('error=>', error);
        });
    }
  };

  useEffect(() => {
    if (history.location.state) {
      let { title, description, _id } = history.location.state;
      setData({ title, description });
      if (_id) {
        setIsEdit(true);
      }
    }
  }, [history.location.state]);
  let isDisabled = data.title.length && data.description.length;
  return (
    <div className="site-card-wrapper">
      <Row gutter={16}>
        <Col
          span={18}
          offset={2}
          style={{ display: 'flex', flexDirection: 'column' }}
        >
          <Card
            title={
              <Input
                onChange={e => setData({ ...data, title: e.target.value })}
                value={data.title}
              />
            }
            bordered={false}
          >
            <Input.TextArea
              value={data.description}
              onChange={e => setData({ ...data, description: e.target.value })}
              autoSize={{ minRows: 5, maxRows: 30 }}
            />
          </Card>
          <Button
            type="primary"
            disabled={!isDisabled}
            style={{ margin: '0 25px' }}
            onClick={handleSubmit}
          >
            Submit
          </Button>
        </Col>
      </Row>
    </div>
  );
}

export default CreatePost;
