import React from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import './index.css';

import { connect } from 'react-redux';
import { AuthSuccessAction } from '../../redux/action/actions';

import { routes } from '../../constants/route';
import { Post,instance } from '../../api/axios';

function Login(props) {
  const onFinish = values => {
    let { email, password } = values;

    Post('/auth', { email, password })
      .then(res => {
        let { username,firstname, isAdmin, _id } = res.data;       
        localStorage.setItem('token', res.headers['x-auth-token']);
        localStorage.setItem(
          'userdata',
          JSON.stringify({ username,firstname, isAdmin, _id }),
        );
        props.AuthSuccessAction(res.data);
        instance.defaults.headers['x-auth-token'] = res.headers['x-auth-token']
      })
      .then(() => {
        props.history.replace(routes.HOME);
      })
      .catch(error => {
        console.log('login error', error);
      });
  };
  return (
    <div className="login-main">
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: 'Please input your email!',
            },
          ]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="email"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your Password!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item className="forgot-main">
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <a className="login-form-forgot" href="/">
            Forgot password
          </a>
        </Form.Item>

        <Form.Item className="register-main">
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
          >
            Log in
          </Button>
          <span className="or">——— OR ———</span>
          <Button
            onClick={() => {
              props.history.replace(routes.REGISTER);
            }}
            type="primary"
            className="login-form-button"
          >
            Register now!
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    userdata: state.loginReducer,
  };
};

export default connect(
  mapStateToProps,
  { AuthSuccessAction },
)(Login);
