import React from 'react';
import { Form, Input, Checkbox, Button, Row, Col, Modal } from 'antd';
import './index.css';


import { routes } from '../../constants/route';
import { Post } from '../../api/axios'

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 12,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const RegistrationForm = props => {
  const [form] = Form.useForm();

  const onFinish = values => {
    let { username, firstname, lastname, email, password } = values
    Post('/user', { username, firstname, lastname, email, password })
      .then((res) => {
        console.log('user info=>', res.data)
        Modal.success({
          content: 'Profile created successfuly.',
          onOk() {
            props.history.replace(routes.LOGIN);
          }
        });
      })
      .catch((err) => {
        console.log('error=>', err)
      })

  };

  return (
    <div className="register-form-main">
      <Form
        {...formItemLayout}
        style={{ width: 'calc(90% - 50px)', marginRight: 20 }}
        form={form}
        name="register"
        onFinish={onFinish}
        scrollToFirstError
      >
        <Form.Item
          name="username"
          label="User Name"
          rules={[
            {
              required: true,
              message: 'Please input your username!',
              whitespace: true,
            },
          ]}
          style={{ marginTop: 20 }}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="firstname"
          label="First Name"
          rules={[
            {
              required: true,
              message: 'Please input your firstname!',
              whitespace: true,
            },
          ]}
          style={{ marginTop: 20 }}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="lastname"
          label="Last Name"
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="email"
          label="E-mail"
          rules={[
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
            {
              required: true,
              message: 'Please input your E-mail!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="password"
          label="Password"
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="confirm"
          label="Confirm Password"
          dependencies={['password']}
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please confirm your password!',
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  'The two passwords that you entered do not match!',
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="agreement"
          valuePropName="checked"
          rules={[
            {
              validator: (_, value) =>
                value
                  ? Promise.resolve()
                  : Promise.reject('Should accept agreement'),
            },
          ]}
          {...tailFormItemLayout}
        >
          <Checkbox>
            I have read the <a href="/">agreement</a>
          </Checkbox>
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button style={{ width: '100%' }} type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <span className="or">——— OR ———</span>
        </Form.Item>
      </Form>
      <Row style={{ width: 'calc(90% - 50px)' }}>
        <Col offset={8} span={16}>
          <Button
            onClick={() => {
              props.history.replace(routes.LOGIN);
            }}
            style={{ width: '100%' }}
            type="primary"
            htmlType="submit"
          >
            Log in
          </Button>
        </Col>
      </Row>
    </div>
  );
};

export default RegistrationForm;
