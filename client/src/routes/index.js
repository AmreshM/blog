import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Dashboard from '../routes/dashboard';
import Login from '../routes/login';
import RegistrationForm from '../routes/register';
import Post from '../components/Post';
import CreatePost from './createpost/'

import { routes } from '../constants/route'

const CustomRoute = () => {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path={routes.HOME} component={Dashboard} />
          <Route exact path={routes.REGISTER} component={RegistrationForm} />
          <Route exact path={routes.LOGIN} component={Login} />
          <Route exact path={`${routes.POST}:id`} component={Post} />
          <Route exact path={`${routes.CREATEPOST}`} component={CreatePost} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default CustomRoute;
