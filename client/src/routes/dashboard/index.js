import React, { useState, useEffect } from 'react';

import Header from '../../components/Header';
import Lists from '../../components/List';
import UserP from '../profile/UserP';
import AdminP from '../profile/AdminP';

import { connect } from 'react-redux';

function Dashboard(props) {
  const [auth, setAuth] = useState({});

  useEffect(() => {
    if (props.data.length) {
      setAuth(props.data);
    } else {
      setAuth(JSON.parse(localStorage.getItem('userdata')));
    }
  }, [props.data]);

  if ((auth && auth.email) || localStorage.getItem('token')) {
    if (!!auth.isAdmin) {
      return <AdminP profile={auth} />;
    } else {
      return <UserP profile={auth} />;
    }
  } else {
    return (
      <div>
        <Header />
        <Lists value='All Posts' />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    data: state.loginReducer,
  };
};

export default connect(mapStateToProps)(Dashboard);
