import React, { useState, useEffect } from 'react';
import { Card, Col, Row, Button } from 'antd';

import { Get, Delete } from '../../api/axios';

import { routes } from '../../constants/route';

function ViewPost(props) {
  const [data, setData] = useState([]);

  useEffect(() => {
    Get(`/posts/${props.id}`)
      .then(res => {
        setData(res.data);
      })
      .catch(error => {
        console.log('error=>', error);
      });
  }, [props.id]);

  const handleEdit = () => {
    console.log(props.history);
    props.history.push(`${routes.CREATEPOST}`, data);
  };

  const handleDelete = () => {
    Delete(`/posts/${props.id}`)
      .then(res => {
        props.history.goBack();
      })
      .catch(error => {
        console.log('error=>', error);
      });
  };
  let isAuthor =
    props.userData && data.author
      ? props.userData._id === data.author._id
      : false;
  let isValidUser = props.userData ? props.userData.isAdmin || isAuthor : null;
  return data.author ? (
    <div className="site-card-wrapper">
      <Row gutter={16}>
        <Col span={18} offset={2}>
          <Card title={data.title} bordered={false}>
            <p>{data.description}</p>
            <span style={{ fontWeight: 'bold' }}>{data.author.firstname}</span>
            <span style={{ fontWeight: 'bold', paddingLeft: 20 }}>
              {new Date(data.date).toUTCString()}
            </span>
          </Card>
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            {isAuthor && (
              <Button
                type="primary"
                style={{ margin: '0 25px' }}
                onClick={handleEdit}
              >
                Edit
              </Button>
            )}
            {isValidUser && (
              <Button
                type="danger"
                style={{ margin: '0 25px' }}
                onClick={handleDelete}
              >
                Delete
              </Button>
            )}
          </div>
        </Col>
      </Row>
    </div>
  ) : (
    <div>
      <p>Something went wrong!</p>
    </div>
  );
}

export default ViewPost;
